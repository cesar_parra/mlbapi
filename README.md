MLB Game Weight API
===================

### Table of contents


[TOC]

What is it?
---------------

An API for getting information about games currently occurring in the MLB. The main focus is to get a sorted list of "Game Situations" weighted by how important they are compared to other game situations occurring at the same time. 

Additionally, it provide methods to get information about all games for a given date, games for a given team 

----------


Endpoints
-------------

Following is a review of available endpoints that can be called through the API.

----------
| HTTP Method| Endpoint | Type | Description   |
| :------- | ----: | :---: |
| GET| `'/' or '/games'`| Games |  Returns all games for today's date    |
| GET    | `'/gameweight`   | Game Situations |  Returns all game situations, sorted from most exciting to least exciting   |
| GET     | `'games/{year}/{month}/{day}'` | Games |  Return all games for the given date|
| GET     | `'games/{year}/{month}/{day}/{team_code}'` | Games |  Return all games for the given date for the specified team by 3 letter team code|
| GET| `'/playertracker'`| Trackers |  Returns information about all players currently in a particular situation (current pitcher, opposing pitcher, current batter, batter on deck, batter in hole)   |

Response format
-------------

**Games**

      {"games": [
	    {
	      "away_team_code": "mia",
	      "away_team_score": "2",
	      "home_team_code": "was",
	      "home_team_score": "4",
	      "inning": "",
	      "status": "Final",
	      "top_inning": false
	    },
	    {
	      "away_team_code": "atl",
	      "away_team_score": "0",
	      "home_team_code": "nyn",
	      "home_team_score": "6",
	      "inning": "",
	      "status": "Final",
	      "top_inning": false
	    }
	    ]
	  }

**Game Situations**

    {
    "games": [
		    {
		      "away_team_code": "kca",
		      "away_team_hits": "2",
		      "away_team_runs": "1",
		      "home_team_code": "min",
		      "home_team_hits": "5",
		      "home_team_runs": "2",
		      "inning": "7",
		      "outs": "2",
		      "runner_on_base_status": "7",
		      "status": "In Progress",
		      "trackers": [
		        {
		          "player_first_name": "Joe",
		          "player_id": "408045",
		          "player_last_name": "Mauer",
		          "status": "current_batter"
		        },
		        {
		          "player_first_name": "Travis",
		          "player_id": "475243",
		          "player_last_name": "Wood",
		          "status": "current_pitcher"
		        },
		        {
		          "player_first_name": "Miguel",
		          "player_id": "593934",
		          "player_last_name": "Sano",
		          "status": "current_ondeck"
		        },
		        {
		          "player_first_name": "Jason",
		          "player_id": "488771",
		          "player_last_name": "Castro",
		          "status": "current_inhole"
		        },
		        {
		          "player_first_name": "Ervin",
		          "player_id": "429722",
		          "player_last_name": "Santana",
		          "status": "opposing_pitcher"
		        }
		      ]
			}
		]
	}

**Trackers**

    {
	    "playertracker": [
		    {
		      "player_first_name": "Justin",
		      "player_id": "475253",
		      "player_last_name": "Smoak",
		      "status": "current_batter"
		    }
		]
	}

Game Weight
-------------

The weight of each game is calculated based on the particular situation going on in the game. Each situation is multiplied by the situation's multiplier. As a simple example: a no-hitter becomes increasingly important as innings pass.

Situations currently being taken into account include:

 - How close is the game
 - How many men are on base
 - Current inning
 - How close is the score
 - Is a no-hitter going

Games that have ended are weighted negatively and games that have not started have a value of 0.

Future
----------

- Additional game situations will be taken into consideration (like how good of a season a player involved is having)
- Additional unit tests will be added
- Additional information about why each game situation is sorted will be returned, alongside the calculated weight.