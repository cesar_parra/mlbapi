from flask import Flask, jsonify
from classes.miniscoreboard import MiniScoreboard
from classes.playertracker import PlayerTracker
from classes.grid import Grid
from classes.gameweightservice import GameWeightService

app = Flask(__name__)


@app.route('/gameweight')
def get_game_weight_for_ongoing_games():
    '''
    Gets a sorted list of games from most exciting to least exciting
    :return: List of game situations
    '''
    player_tracker = PlayerTracker()
    game_situations_for_today = player_tracker.get_game_situation_data(*__get_current_date())
    game_weight_calculator = GameWeightService()
    sorted_situations = game_weight_calculator.get_sorted_game_situations(game_situations_for_today)
    return jsonify(games=[g.serialize for g in sorted_situations])

@app.route('/')
@app.route('/games')
def get_games_for_today():
    '''
    Gets all games for today
    :return:  List of games for today
    '''
    return jsonify(games=[g.serialize for g in Grid.get_game_data(*__get_current_date())])


@app.route('/games/<string:year>/<string:month>/<string:day>')
def get_games(year, month, day):
    '''
    Gets all games on the specified date
    :param year: The year the games occurred
    :param month: The month the games occurred
    :param day: The day the games occurred
    :return: List of games
    '''
    return jsonify(games=[g.serialize for g in Grid.get_game_data(year, month, day)])


@app.route('/games/<string:year>/<string:month>/<string:day>/<string:team_code>')
def get_games_for_team(year, month, day, team_code):
    '''
    Gets all games on the specified date for the specified team
    :param year: The year the games occurred
    :param month: The month the games occurred
    :param day: The day the games occurred
    :param team_code: The team we are searching a game for
    :return: Game
    '''
    games_for_team = [current_game for current_game
                      in MiniScoreboard.get_game_data(year, month, day)
                      if current_game.home_team_code == team_code or current_game.away_team_code == team_code]
    return jsonify(games=[g.serialize for g in games_for_team])


@app.route('/playertracker')
def get_tracker_data():
    player_tracker = PlayerTracker()
    return jsonify(playertracker=[t.serialize for t in player_tracker.get_tracker_data(*__get_current_date())])


def __get_current_date():
    import datetime
    now = datetime.datetime.now()
    year = str(now.year)
    month = '0' + str(now.month) if now.month < 10 else str(now.month)
    day = '0' + str(now.day) if now.day < 10 else str(now.day)

    return year, month, day


def __get_game_data_implementation():
    # return MiniScoreboard()
    # using the Grid implementation since that has some additional information
    return Grid()

if __name__ == '__main__':
    # app.debug = True
    # app.run(host='0.0.0.0', port=5000)
    # # port = int(os.environ.get("PORT", 5000))
    # # app.run(host='0.0.0.0', port=port)
    app.run()

