class GameWeightService(object):
    '''
    Service to calculate game weights and to sort a list of ongoing game situations.
    '''
    def __init__(self):
        self.RUNNER_ON_BASE_WEIGHT = 2
        self.SCORE_DIFFERENT_WEIGHT = 5
        self.NO_HITTER_MULTIPLIER = 2
        self.INNING_MULTIPLIER = 1.5

    def get_sorted_game_situations(self, game_situations):
        '''
        Sorts game situations from highest weight to lowest weight
        :param game_situations: List of game situations to sort
        :return: Sorted list of game situations
        '''
        # Dictionary of game weight by list of game situations with that weight
        game_sit_by_weight = {}
        for game_situation in game_situations:
            situation_weight = self.calculate_weight(game_situation)
            if situation_weight not in game_sit_by_weight:
                game_sit_by_weight[situation_weight] = []

            situations_in_position = game_sit_by_weight[situation_weight]
            situations_in_position.append(game_situation)
            game_sit_by_weight[situation_weight] = situations_in_position

        game_weights = game_sit_by_weight.keys()
        game_weights = sorted(game_weights, reverse=True)

        sorted_situations = list()
        for current_weight in game_weights:
            sorted_situations.extend(game_sit_by_weight.get(current_weight))

        return sorted_situations

    def calculate_weight(self, game_situation):
        '''
        Calculates the weight of the passed in game situation.
        :param game_situation: GameSituation to calculate the weight for.
        :return: Numeric weight for the game situation.
        '''
        if game_situation.is_game_over():
            return -1

        if not game_situation.in_progress():
            return 0

        def _get_men_on_base_weight():
            if game_situation.runner_on_base_status == '':
                return 0
            return int(game_situation.runner_on_base_status) * self.RUNNER_ON_BASE_WEIGHT

        def _get_inning_situation_weight():
            return int(game_situation.inning) * self.INNING_MULTIPLIER

        def _get_team_standing_weight():
            return 0

        def _get_score_weight():
            '''
            Weight based on how close the score is
            :return: 
            '''
            home_score = int(game_situation.home_team_runs)
            away_score = int(game_situation.away_team_runs)
            run_difference = abs(home_score - away_score)

            # Score starts at 10 and decreases the more one team is beating the other,
            # even going into the negatives if a team has over 10 runs over the other
            return self.SCORE_DIFFERENT_WEIGHT - run_difference

        def _get_current_pitcher_weight():
            # TODO: How good has this pitcher been this season?
            return 0

        def _get_opposing_pitcher_weight():
            # TODO: How good has this pitcher been this season?
            return 0

        def _get_current_batter_weight():
            # TODO: How good has the batter been this season?
            return 0

        def _get_current_on_deck_weight():
            # TODO: How good has the batter been this season?
            return 0

        def _get_current_in_hole_weight():
            # TODO: How good has the batter been this season?
            return 0

        def _get_no_hitter_weight():
            home_team_hits = int(game_situation.home_team_hits)
            away_team_hits = int(game_situation.away_team_hits)

            if home_team_hits != 0 and away_team_hits != 0:
                return 0

            inning = int(game_situation.inning)

            # We don't care too much about a no hitter going in the first inning
            if inning == 1:
                return 0

            return inning * self.NO_HITTER_MULTIPLIER

        return _get_men_on_base_weight() + _get_inning_situation_weight() + \
            _get_team_standing_weight() + _get_current_pitcher_weight() + \
            _get_opposing_pitcher_weight() + _get_current_batter_weight() + \
            _get_current_on_deck_weight() + _get_current_in_hole_weight() + \
            _get_score_weight() + _get_no_hitter_weight()

