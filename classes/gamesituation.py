class GameSituation(object):
    def __init__(self):
        self.trackers = list()
        self.runner_on_base_status = ''
        self.home_team_runs = '0'
        self.away_team_runs = '0'
        self.home_team_code = ''
        self.away_team_code = ''
        self.status = ''
        self.inning = ''
        self.outs = ''
        self.home_team_hits = ''
        self.away_team_hits = ''

    def is_game_over(self):
        '''
        Whether the game is over or not.
        :return: Boolean indicating if the game is over.
        '''
        return self.status == 'Final'

    def in_progress(self):
        '''
        Whether the game is in progress or not.
        :return: Boolean indicating if the game is in progress.
        '''
        return self.status == 'In Progress'

    @property
    def serialize(self):
        return {
            'trackers': [t.serialize for t in self.trackers],
            'runner_on_base_status': self.runner_on_base_status,
            'home_team_runs': self.home_team_runs,
            'away_team_runs': self.away_team_runs,
            'home_team_code': self.home_team_code,
            'away_team_code': self.away_team_code,
            'status': self.status,
            'inning': self.inning,
            'outs': self.outs,
            'home_team_hits': self.home_team_hits,
            'away_team_hits': self.away_team_hits,
        }

