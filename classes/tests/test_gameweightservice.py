from classes.gameweightservice import GameWeightService
from classes.gamesituation import GameSituation
import unittest

class GameWeightTestCase(unittest.TestCase):

    def test_get_sorted_game_situations_expectCorrectSort(self):
        game_over_situation = GameSituation()
        game_over_situation.status = 'Final'

        game_not_started_situation = GameSituation()
        game_not_started_situation.status = 'Preview'

        no_hitter_situation = GameSituation()
        no_hitter_situation.status = 'In Progress'
        no_hitter_situation.inning = '5'
        no_hitter_situation.home_team_hits = '0'
        no_hitter_situation.away_team_hits = '5'

        test_situations = [game_over_situation, game_not_started_situation, no_hitter_situation]

        calculator = GameWeightService()
        sorted_situations = calculator.get_sorted_game_situations(test_situations)

        assert sorted_situations[0].status == 'In Progress'
        assert sorted_situations[1].status == 'Preview'
        assert sorted_situations[2].status == 'Final'

    def test_calculate_weight_gameOver_expectMinusOne(self):
        game_over_situation = GameSituation()
        game_over_situation.status = 'Final'

        calculator = GameWeightService()

        result = calculator.calculate_weight(game_over_situation)

        assert result == -1

    def test_calculate_weight_gameNotStarted_expectZero(self):
        game_not_started_situation = GameSituation()
        game_not_started_situation.status = 'Preview'

        calculator = GameWeightService()

        result = calculator.calculate_weight(game_not_started_situation)

        assert result == 0

    def test_calculate_weight_gameInProgress_expectGreaterThanZero(self):
        no_hitter_situation = GameSituation()
        no_hitter_situation.status = 'In Progress'
        no_hitter_situation.inning = '5'
        no_hitter_situation.home_team_hits = '0'
        no_hitter_situation.away_team_hits = '5'

        calculator = GameWeightService()

        result = calculator.calculate_weight(no_hitter_situation)

        assert result > 0