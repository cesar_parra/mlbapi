from classes.game import Game
import unittest

class GameTestCase(unittest.TestCase):

    def test_get_score_newTeam_expectZero(self):
        test_game = Game()
        home_team_score, away_team_score = test_game.get_score()
        assert home_team_score == 0
        assert away_team_score == 0

    def test_get_score_withScores_expectCorrectScores(self):
        test_game = Game()
        expected_home_team_score = 5
        expected_away_team_score = 10

        test_game.home_team_score = expected_home_team_score
        test_game.away_team_score = expected_away_team_score

        home_team_score, away_team_score = test_game.get_score()
        assert home_team_score == expected_home_team_score
        assert away_team_score == expected_away_team_score

    def test_is_game_over_gameOver_expectTrue(self):
        test_game = Game()

        assert not test_game.is_game_over()

    def test_is_game_over_gameNotOver_expectFalse(self):
        test_game = Game()
        test_game.status = 'Final'

        assert test_game.is_game_over()

    def test_in_progress_gameInProgress_expectTrue(self):
        test_game = Game()

        assert not test_game.in_progress()

    def test_in_progress_gameNotInProgress_expectFalse(self):
        test_game = Game()
        test_game.status = 'In Progress'

        assert test_game.in_progress()

