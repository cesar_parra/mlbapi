from classes.gamesituation import GameSituation
import unittest

class GameSituationTestCase(unittest.TestCase):

    def test_is_game_over_gameOver_expectTrue(self):
        game_situation = GameSituation()
        game_situation.status = 'Final'

        assert game_situation.is_game_over()

    def test_is_game_over_gameNotOver_expectFalse(self):
        game_situation = GameSituation()

        assert not game_situation.is_game_over()

    def test_in_progress_gameInProgress_expectTrue(self):
        game_situation = GameSituation()
        game_situation.status = 'In Progress'

        assert game_situation.in_progress()

    def test_in_progress_gameNotInProgress_expectFalse(self):
        game_situation = GameSituation()

        assert not game_situation.in_progress()

