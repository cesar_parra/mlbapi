import classes.grid
import classes.game
import unittest
import httpretty

def get_json_data():
    return '{"data":{"games":{"game":[{"game_type":"R","game_nbr":"1","double_header_sw":"N","away_team_name":"Yankees","id":' + \
           '"2017/04/02/nyamlb-tbamlb-1","home_name_abbrev":"TB","media_state":"media_archive","top_inning":"Y","home_team_name"'+\
           ':"Rays","ind":"F","venue_id":"12","gameday_sw":"P","away_team_id":"147","home_score":"7","status":"Final","home_code":'+\
           '"tba","away_score":"3","inning":"9","game_pk":"490106","away_name_abbrev":"NYY","venue":"Tropicana Field","home_file_code"'+\
           ':"tb","away_file_code":"nyy","event_time":"1:10 PM","home_team_id":"139","calendar_event_id":"14-490106-2017-04-02","group"'+\
           ':"MLB","tbd_flag":"N","away_code":"nya"},{"game_type":"R","game_nbr":"1","double_header_sw":"N","away_team_name":"Giants","id"'+\
           ':"2017/04/02/sfnmlb-arimlb-1","home_name_abbrev":"ARI","media_state":"media_archive","top_inning":"N","home_team_name":"D-backs"'+\
           ',"ind":"F","venue_id":"15","gameday_sw":"P","away_team_id":"137","home_score":"6","status":"Final","home_code":"ari","away_score":'+\
           '"5","inning":"9","game_pk":"490110","away_name_abbrev":"SF","venue":"Chase Field","home_file_code":"ari","away_file_code":"sf",'+\
           '"event_time":"4:10 PM","home_team_id":"109","calendar_event_id":"14-490110-2017-04-02","group":"MLB","tbd_flag":"N","away_code":'+\
           '"sfn"}],"date":"20170402"}}}'

class GridTestCase(unittest.TestCase):
    @httpretty.activate
    def test_get_game_data_badResponse_expectEmptyGenerator(self):
        httpretty.register_uri(httpretty.GET, "http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_03/grid.json",
                               status=500)

        test_grid = classes.grid.Grid()
        response = test_grid.get_game_data('2017', '04', '03')

        for game in response:
            assert False

    @httpretty.activate
    def test_get_game_data_goodResponse_expectGamesGenerated(self):
        httpretty.register_uri(httpretty.GET,
                               "http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_03/grid.json",
                               status=200, body=get_json_data())

        test_grid = classes.grid.Grid()
        response = test_grid.get_game_data('2017', '04', '03')

        found_games = 0
        for game in response:
            assert isinstance(game, classes.game.Game)
            found_games = found_games + 1

        assert found_games == 2
