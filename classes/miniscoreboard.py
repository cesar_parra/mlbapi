from game import Game
import httplib2
import json


class MiniScoreboard(object):
    '''
    Handles communicating with the miniscoreboard data of the MLB API
    '''
    @staticmethod
    def get_game_data(year, month, day):
        '''
        Generator class for looping over games returned from the miniscoreboard data of the MLB API.
        :param year: Year to get the game data from
        :param month: Month to get the game data from
        :param day: Day to get the game data from
        :return: Generator to iterate over game data
        '''
        url = r"http://gd2.mlb.com/components/game/mlb/year_%s/month_%s/day_%s/miniscoreboard.json" % (year, month, day)

        http_session = httplib2.Http()
        # TODO: Check that the 'status' is 200 by getting index 0
        scoreboard = json.loads(http_session.request(url, "GET")[1])

        for json_game in scoreboard['data']['games']['game']:
            current_game = Game()
            current_game.home_team_code = json_game['home_code']
            current_game.away_team_code = json_game['away_code']
            if 'home_team_runs' in json_game and 'away_team_runs' in json_game:
                current_game.home_team_score = json_game['home_team_runs']
                current_game.away_team_score = json_game['away_team_runs']
            else:
                current_game.home_team_score = 0
                current_game.away_team_score = 0
            yield current_game

