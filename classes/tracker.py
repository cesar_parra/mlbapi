class Tracker(object):
    player_id = ''
    player_first_name = ''
    player_last_name = ''

    # TODO: Add a boolean method to identify each status for easy calling
    # Could be: Current Pitcher, Opposing Pitcher, Current Batter, Current On Deck, Current In Hole
    status = ''

    @property
    def serialize(self):
        return {
            'player_id': self.player_id,
            'player_first_name': self.player_first_name,
            'player_last_name': self.player_last_name,
            'status': self.status,
        }