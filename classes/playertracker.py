import httplib2
import xml.etree.ElementTree as ET
from tracker import Tracker
import itertools
from gamesituation import GameSituation


class PlayerTracker(object):
    def __init__(self):
        self.url = "http://gd2.mlb.com/components/game/mlb/year_%s/month_%s/day_%s/playertracker.xml"

    def get_tracker_data(self, year, month, day):
        url = self.url % (year, month, day)

        http_session = httplib2.Http()
        # TODO: Check that the 'status' is 200 by getting index 0
        game_tracker = ET.fromstring(http_session.request(url, "GET")[1])

        tracker_list = itertools.chain(game_tracker.iter('current_batter'), game_tracker.iter('current_pitcher'),
                                       game_tracker.iter('current_ondeck'), game_tracker.iter('current_inhole'),
                                       game_tracker.iter('opposing_pitcher'))

        for current_game in tracker_list:
            current_tracker = Tracker()
            current_tracker.player_first_name = current_game.attrib['first_name']
            current_tracker.player_last_name = current_game.attrib['last_name']
            current_tracker.player_id = current_game.attrib['id']
            current_tracker.status = current_game.tag
            yield current_tracker

    def get_game_situation_data(self, year, month, day):
        url = self.url % (year, month, day)

        http_session = httplib2.Http()
        # TODO: Check that the 'status' is 200 by getting index 0
        xml_data = http_session.request(url, "GET")[1]
        game_tracker = ET.fromstring(xml_data)

        game_situations = list()
        for current_game in game_tracker.iter('game'):
            current_situation = GameSituation()
            # TODO: Check that everything that I'm getting is actually on the dictionary
            if 'runner_on_base_status' in current_game.attrib:
                current_situation.runner_on_base_status = current_game.attrib['runner_on_base_status']
            if 'home_team_runs' in current_game.attrib:
                current_situation.home_team_runs = current_game.attrib['home_team_runs']
            if 'away_team_runs' in current_game.attrib:
                current_situation.away_team_runs = current_game.attrib['away_team_runs']
            current_situation.home_team_code = current_game.attrib['home_code']
            current_situation.away_team_code = current_game.attrib['away_code']
            current_situation.status = current_game.attrib['status']
            if 'inning' in current_game.attrib:
                current_situation.inning = current_game.attrib['inning']
            if 'outs' in current_game.attrib:
                current_situation.outs = current_game.attrib['outs']
            if 'away_team_hits' in current_game.attrib:
                current_situation.away_team_hits = current_game.attrib['away_team_hits']
            if 'home_team_hits' in current_game.attrib:
                current_situation.home_team_hits = current_game.attrib['home_team_hits']

            tracker_list = itertools.chain(current_game.iter('current_batter'), current_game.iter('current_pitcher'),
                                           current_game.iter('current_ondeck'), current_game.iter('current_inhole'),
                                           current_game.iter('opposing_pitcher'))

            for track in tracker_list:
                current_tracker = Tracker()
                current_tracker.player_first_name = track.attrib['first_name']
                current_tracker.player_last_name = track.attrib['last_name']
                current_tracker.player_id = track.attrib['id']
                current_tracker.status = track.tag
                current_situation.trackers.append(current_tracker)

            game_situations.append(current_situation)

        return game_situations
