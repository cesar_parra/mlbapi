class Game(object):
    """
    Represents a game.
    """

    def __init__(self):
        self.home_team_code = ''
        self.away_team_code = ''

        self.home_team_score = 0
        self.away_team_score = 0

        self.status = ''
        self.inning = ''
        self.top_inning = False

    def get_score(self):
        '''
        Gets the score of the game as a tuple of: home team score, away team score
        :return: Tuple of home team score, away team score
        '''
        return self.home_team_score, self.away_team_score

    def is_game_over(self):
        '''
        Whether the game is over or not.
        :return: Boolean indicating if the game is over or not.
        '''
        return self.status == 'Final'

    def in_progress(self):
        '''
        Whether the game is in progress or not.
        :return: Boolean indicating if the game is in progress or not.
        '''
        return self.status == 'In Progress'

    @property
    def serialize(self):
        return {
            'home_team_code': self.home_team_code,
            'away_team_code': self.away_team_code,
            'home_team_score': self.home_team_score,
            'away_team_score': self.away_team_score,
            'status': self.status,
            'inning': self.inning,
            'top_inning': self.top_inning,
        }

