from game import Game
import httplib2
import json


class Grid(object):
    @staticmethod
    def get_game_data(year, month, day):
        '''
        Handles communicating with the grid data from the MLB API
        :param year: Year to get the game data from
        :param month: Month to get the game data from
        :param day: Day to get the game data from
        :return: Generator to iterate over game data
        '''
        url = r"http://gd2.mlb.com/components/game/mlb/year_%s/month_%s/day_%s/grid.json" % (year, month, day)

        http_session = httplib2.Http()
        response = http_session.request(url, "GET")
        if response[0].status != 200 and response[0].status != '200':
            return
        scoreboard = json.loads(response[1])

        for json_game in scoreboard['data']['games']['game']:
            current_game = Game()
            current_game.home_team_code = json_game['home_code']
            current_game.away_team_code = json_game['away_code']
            if 'home_score' in json_game and 'away_score' in json_game:
                current_game.home_team_score = json_game['home_score']
                current_game.away_team_score = json_game['away_score']
            else:
                current_game.home_team_score = 0
                current_game.away_team_score = 0
            current_game.status = json_game['status']
            current_game.inning2 = json_game['inning']
            if 'top_inning' in json_game:
                current_game.top_inning = True if json_game['top_inning'] == "N" else False
            yield current_game

